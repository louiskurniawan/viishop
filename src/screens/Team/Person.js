import React, { Component } from 'react';
import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  View,
  Spinner,
  Thumbnail,
} from "native-base";
import styles           from "./styles";
import {Grid, Col, Row} from "react-native-easy-grid";
import config           from "../../config";
const primary = require("../../theme/variables/commonColor").brandPrimary;

const Person = props => {
  const { name, position, image } = props;
  const imageSource = `${config.baseURL}storage/app/images/${image}`;
  return (
    <View style={styles.box}>
      <Thumbnail square source={{uri: imageSource}} />
      <Text style={[styles.primaryColor, styles.centerText]}>
        {name}
      </Text>
      <Text style={[styles.desc, styles.centerText]}>
        {position}
      </Text>
    </View>
  );
};

export default Person;
