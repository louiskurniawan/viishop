import React, {Component} from "react";
import {
  Dimensions,
} from "react-native";
import {connect} from "react-redux";
import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  View,
  Spinner,
  Title,
} from "native-base";

import { Grid, Row } from "react-native-easy-grid";

import { fetchTeam } from "./actions";
import datas  from "./data";
import styles from "./styles";
import Person from "./Person";
const primary = require("../../theme/variables/commonColor").brandPrimary;

const deviceWidth = Dimensions.get("window").width;
class Team extends Component {
  componentDidMount() {
    this.props.fetchTeam();
  }
  render() {
    console.log(this.props)
    if (this.props.isLoading) {
      return <Spinner />;
    } else {
      return (
        <Container style={styles.container}>
          <Header>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              >
                <Icon active name="menu" />
              </Button>
            </Left>
            <Body>
              <Title style={styles.headerTitle}>Team</Title>
            </Body>
            <Right/>
          </Header>
          <Content padder>

            <View >
              <Text style={styles.newsHeader}>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </Text>
              <Grid>
                <Row style={styles.list}>
                  {
                    this.props.teams.data 
                      ? this.props.teams.data.map((result)=>{
                          return <Person key={result.id} {...result}/>
                        })
                      : null
                  }
                </Row>
              </Grid>
            </View>
          </Content>
        </Container>
      );

    }
  }
}
function bindAction(dispatch){
  return {
    fetchTeam : () => dispatch(fetchTeam()),
  };
};
const mapStateToProps = (state) => {
  return {
    teams : state.teamReducer.teams,
    hasErrored : state.teamReducer.hasErrored,
    isLoading : state.teamReducer.isLoading,
  };
};
export default connect(mapStateToProps, bindAction)(Team);
