
import React, {Component} from "react";
import {
  Platform,
  Image,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
  FlatList,
  View as RNView
} from "react-native";

import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  View,
  Title,
  Spinner
} from "native-base";
import Carousel     from "react-native-carousel-view";
import {Grid, Col, Row} from "react-native-easy-grid";

import datas from "./data.js";
import styles from "./styles";
import Feature from "./Feature";

const primary = require("../../theme/variables/commonColor").brandPrimary;
const deviceWidth = Dimensions.get("window").width;
const headerLogo = require("../../../assets/header-logo.png");

class Main extends Component {
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
            >
              <Icon active name="menu" />
            </Button>
          </Left>
          <Body>
            <Image source={headerLogo} style={styles.imageHeader} />
          </Body>
          <Right />
        </Header>
        <Content
          showsVerticalScrollIndicator={false}
          style={{backgroundColor: "#fff"}}
        >
          <Carousel
            width={deviceWidth}
            height={330}
            indicatorAtBottom
            indicatorSize={Platform.OS === "android" ? 15 : 10}
            indicatorColor="#FFF"
            indicatorOffset={10}
            animate={false}
          >
            <RNView>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => this.props.navigation.navigate("Story")}
                style={styles.slide}
              >
                <ImageBackground
                  style={styles.newsPoster}
                  source={require("../../../assets/NewsIcons/1.jpg")}
                >
                  <View style={styles.swiperTextContent}>
                    <Text
                      numberOfLines={2}
                      style={styles.newsPosterHeader}
                    >
                      Flat App is a style of interface design emphasizing
                      minimal use of stylistic elements.
                    </Text>
                    <Grid style={styles.swiperContentBox}>
                      <Col style={{flexDirection: "row"}}>
                        <Text style={styles.newsPosterLink}>
                          SPACE.com
                        </Text>
                        <Icon
                          name="ios-time-outline"
                          style={styles.timePosterIcon}
                        />
                        <Text style={styles.newsPosterLink}>20m ago</Text>
                      </Col>
                      <Col>
                        <TouchableOpacity
                          style={styles.newsPosterTypeView}
                        >
                          <Text
                            style={styles.newsPosterTypeText}
                            onPress={() =>
                              this.props.navigation.navigate("Channel")}
                          >
                            SCIENCE
                          </Text>
                        </TouchableOpacity>
                      </Col>
                    </Grid>
                  </View>
                </ImageBackground>
              </TouchableOpacity>
            </RNView>
            <RNView>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => this.props.navigation.navigate("Story")}
                style={styles.slide}
              >
                <ImageBackground
                  style={styles.newsPoster}
                  source={require("../../../assets/NewsIcons/3.jpg")}
                >
                  <View style={styles.swiperTextContent}>
                    <Text
                      numberOfLines={2}
                      style={styles.newsPosterHeader}
                    >
                      So that the applications are able to load faster and
                      resize easily.
                    </Text>
                    <Grid style={styles.swiperContentBox}>
                      <Col style={{flexDirection: "row"}}>
                        <Text style={styles.newsPosterLink}>CDC</Text>
                        <Icon
                          name="ios-time-outline"
                          style={styles.timePosterIcon}
                        />
                        <Text style={styles.newsPosterLink}>2hr ago</Text>
                      </Col>
                      <Col>
                        <TouchableOpacity
                          style={styles.newsPosterTypeView}
                        >
                          <Text
                            style={styles.newsPosterTypeText}
                            onPress={() =>
                              this.props.navigation.navigate("Channel")}
                          >
                            ENVIRONMENT
                          </Text>
                        </TouchableOpacity>
                      </Col>
                    </Grid>
                  </View>
                </ImageBackground>
              </TouchableOpacity>
            </RNView>
            <RNView>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => this.props.navigation.navigate("Story")}
                style={styles.slide}
              >
                <ImageBackground
                  style={styles.newsPoster}
                  source={require("../../../assets/NewsIcons/4.jpg")}
                >
                  <View style={styles.swiperTextContent}>
                    <Text
                      numberOfLines={2}
                      style={styles.newsPosterHeader}
                    >
                      But still look sharp on high-definition screens.
                    </Text>
                    <Grid style={styles.swiperContentBox}>
                      <Col style={{flexDirection: "row"}}>
                        <Text style={styles.newsPosterLink}>SKY.com</Text>
                        <Icon
                          name="ios-time-outline"
                          style={styles.timePosterIcon}
                        />
                        <Text style={styles.newsPosterLink}>
                          1day ago
                        </Text>
                      </Col>
                      <Col>
                        <TouchableOpacity
                          style={styles.newsPosterTypeView}
                        >
                          <Text
                            style={styles.newsPosterTypeText}
                            onPress={() =>
                              this.props.navigation.navigate("Channel")}
                          >
                            WORLD
                          </Text>
                        </TouchableOpacity>
                      </Col>
                    </Grid>
                  </View>
                </ImageBackground>
              </TouchableOpacity>
            </RNView>
          </Carousel>
          <Grid padder>
            <Row style={styles.list}>
              {
                datas.map((result) => {
                  return <Feature key={result.id} {...result}/>
                })
              }
            </Row>
          </Grid>

          <Grid style={styles.gridContact}>
            <Title style={styles.title}>Contact</Title>
            <Row style={styles.rowContact}>
              <View style={styles.boxIcon}>
                <Icon name="call" fontSize={50} style={styles.icon}/>
              </View>

              <View style={[styles.boxIcon, {backgroundColor:'#fff'}]}>
                <Icon type="FontAwesome" name="envelope"  fontSize={50} style={[styles.icon,{backgroundColor:'white', color:primary}]}/>
              </View>

              <View style={styles.boxIcon}>
                <Icon name="pin"  fontSize={50} style={styles.icon}/>
              </View>
            </Row>
            <Title style={[styles.title,{marginBottom:20, fontWeight:'100'}]}>Lorem ipsum</Title>
          </Grid>


          <Grid style={styles.gridFollow}>
            <Title style={[styles.title, { color : primary }]}>Follow</Title>
            <Row style={styles.rowContact}>
              <View style={[styles.boxIcon, {backgroundColor:primary}]}>
                <Icon name="logo-instagram"  fontSize={50} style={[styles.icon,{backgroundColor:primary, color:'white'}]}/>
              </View>

              <View style={[styles.boxIcon, {backgroundColor:primary}]}>
                <Icon name="logo-instagram"  fontSize={50} style={[styles.icon,{backgroundColor:primary, color:'white'}]}/>
              </View>

              <View style={[styles.boxIcon, {backgroundColor:primary}]}>
                <Icon name="logo-instagram"  fontSize={50} style={[styles.icon,{backgroundColor:primary, color:'white'}]}/>
              </View>
            </Row>
          </Grid>
        </Content>
      </Container>
    );
  }
}

export default Main;
