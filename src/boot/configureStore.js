// @flow
import { AsyncStorage } from "react-native";
import devTools         from "remote-redux-devtools";
import thunk            from "redux-thunk";
import storage          from "redux-persist/lib/storage";
import rootReducer      from "../reducers";
import {
    persistStore,
    persistReducer,
} from "redux-persist";
import reducer          from "../reducers";
import {
   createStore,
   applyMiddleware,
   compose
} from "redux";

export default function configureStore(onCompletion: () => void): any {
    const enhancer = compose(
        applyMiddleware(thunk),
        devTools({
            name: "flatapp",
            realtime: true
        })
    );
    const persistConfig = {
        key : "root",
        storage,
    };

    const persistedReducer = persistReducer(persistConfig, rootReducer)
    const store = createStore(persistedReducer, enhancer);
    const persistor = persistStore(store, onCompletion);

    return { store , persistor};
}
